var Utils = {
    upFirstChar: function(str) {
        return (str.charAt(0).toUpperCase() + str.slice(1));
    },

    setVisible: function(element, value) {
        if (value) {
            element.style.visibility = "visible";
        } else {
            element.style.visibility = "hidden";
        }
    }
}