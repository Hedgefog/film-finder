var FilmComponent = Component.Create({
    template:   `<div class="masonry-item">
                    <img class="f-image" src="{Poster}"></img>
                    <p class="f-title">{Title}</p>
                    <p class="f-description">{Year}, {Type}</p>
                    <div class="f-actions"></div>
                </div>`,

    onInit(data) {
        this.imdbID = data.imdbID;
        this.invalidImage = (data.Poster == "N/A");
    },

    buildElement: function() {
        var element = Component.prototype.buildElement.apply(this);
        this.createActionBlock(element);

        if (this.invalidImage) {
            var imageElement = element.querySelector(".f-image");
            imageElement.remove();
        }

        return element;
    },

    createActionBlock: function(element) {
        var actionsBlock = element.querySelector(".f-actions");
        actionsBlock.appendChild(
            this.createFavoriteAction(element)
        );
    },

    createFavoriteAction: function(element) {
        var action = document.createElement("div");
        action.className = "f-a-favorite";
        action.imdbID = element.getAttribute("imdbID");

        var self = this;
        action.onclick = function(event) {
            self.toggleFavorite();
            var isFavorite = Favorites.check(self.imdbID);
            self.updateFavoriteAction(action);
        }

        this.updateFavoriteAction(action);

        return action;
    },

    markFavorite: function(action, value) {
        if (value) {
            action.classList.add("checked");
        } else {
            action.classList.remove("checked");
        }
    },

    toggleFavorite: function() {
        Favorites.toggle(this.imdbID);
    },

    updateFavoriteAction: function(action) {
        var isFavorite = Favorites.check(this.imdbID);
        if (isFavorite) {
            action.classList.add("checked");
        } else {
            action.classList.remove("checked");
        }
    }
});