var FilmFinder = {
    EndPoints: {
        Base: "http://www.omdbapi.com"
    },

    Plot: {
        Short: "short",
        Full: "full"
    },

    byName: function(title, plot, page, callback, error_callback) {
        if (!title) {
            return;
        }

        plot = plot || Plot.Short;

        var params = {
            s: title,
            plot: plot,
            r: "json"
        }

        if (page) {
            params.page = page;
        }

        Request.make(this.EndPoints.Base, Request.Type.Post, params, function(response) {
            if (!response) {
                return;
            }

            var json = JSON.parse(response);

            if (!json) {
                return;
            }
            
            if (error_callback && json.Response == "False") {
                error_callback(json.Error);
                return;
            }

            if (!json.Search) {
                return;
            }

            if (callback) {
                callback(json.Search, parseInt(json.totalResults));
            }
        });
    },
    
    byId : function(imdbID, plot, callback, error_callback) {
        if (!imdbID) {
            return;
        }

        plot = plot || Plot.Short;

        var params = {
            i: imdbID,
            plot: plot,
            r: "json"
        }

        Request.make(this.EndPoints.Base, Request.Type.Post, params, function(response) {
            if (!response) {
                return;
            }

            var json = JSON.parse(response);

            if (!json) {
                return;
            }

            if (error_callback && json.Response == "False") {
                error_callback(json.Error);
                return;
            }

            if (callback) {
                callback(json);
            }
        });
    }
}



