
var Request = {
    Type: {
        Post: "POST",
        Get: "GET"
    },

    toParameters: function(object) {
        var params = "";

        var keys = Object.keys(object);
        for (var i = 0; i < keys.length; ++i) {
            var key = keys[i];

            if (i == 0) {
                params += "?";
            } else {
                params += "&";
            }

            params += (key + "=" + encodeURIComponent(object[key]));
        }

        return params;
    },

    make: function(url, method, params, callback) {
        var encoded_params = this.toParameters(params);
        console.debug(url + encoded_params);

        var request = new XMLHttpRequest();
        request.open(method, url + encoded_params);
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                if (request.status == 200) {
                    callback(request.responseText);
                }
            }
        }
        
        request.send();
    }
}
