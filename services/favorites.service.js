var Favorites = {
    list: [],

    add: function(imdbID) {
        this.list.push(imdbID);

        this.export();
    },

    remove: function(imdbID) {
        var index = this.list.indexOf(imdbID);
        this.list.splice(index);

        this.export();
    },

    toggle: function(imdbID) {
        var index = this.list.indexOf(imdbID);
        if (index < 0) {
            this.list.push(imdbID);
        } else {
            this.list.splice(index, 1);
        }

        console.log(this.list);
        this.export();
    },

    check: function(imdbID) {
        return (this.list.indexOf(imdbID) >= 0);
    },

    indexOf: function(imdbID) {
        return this.list.indexOf(imdbID);
    },

    import: function() {
        if (window.localStorage.hasOwnProperty("favorites")) {
            var str = window.localStorage.getItem("favorites");
            var arr = JSON.parse(str);
            this.list = arr;
        }
    },

    export: function() {
        var str = JSON.stringify(this.list);
        window.localStorage.setItem("favorites", str);
    }
};

Favorites.import();