function Component(data) {}

Component.prototype.buildElement = function() {
    var element = document.createElement("div");
    element.innerHTML = this.template;
    element = element.firstChild;
    
    return element;
};

Component.Create = function(componentData) {
    function component(data) {
        if (this.template) {
            for (key in data) {
                var field = "{" + key + "}";
                this.template = this.template.replace(field, data[key]);
            }
        }

        this.onInit(data);
    }

    component.prototype = Object.create(Component.prototype);
    component.prototype.constructor = component;
    
    for (key in componentData) {
        component.prototype[key] = componentData[key];
    }

    return component;
};