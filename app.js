var g_filmsBlock;
var g_searchInput;
var g_searchButton;
var g_loadMoreButton;
var g_errorBlock;

var g_lastTitle = "";
var g_currentPage = 0;
var g_filmsLeft = 0;

window.onload = function() {
    g_filmsBlock = document.querySelector("#films-list");
    g_searchInput = document.querySelector("#search-input");
    g_searchButton = document.querySelector("#search-button");
    g_loadMoreButton = document.querySelector("#load-more-button");
    g_errorBlock = document.querySelector(".error-block");

    g_searchButton.onclick = onSearchButtonClick;
    g_loadMoreButton.onclick = onLoadMore;
    g_searchInput.onkeypress = onSearchInputKeyPress;

    Utils.setVisible(g_loadMoreButton, false);
    Utils.setVisible(g_errorBlock, false);

    loadFavorites();
}

/*----------------[Loaders]----------------*/

function loadFavorites() {
    clearPage();

    for (var i = 0; i < Favorites.list.length; ++i) {
        FilmFinder.byId(Favorites.list[i], FilmFinder.Plot.Short, function(film) {
            film.Type = Utils.upFirstChar(film.Type);

            var component = new FilmComponent(film);
            var element = component.buildElement();

            g_filmsBlock.appendChild(element);
        }, showError);
    }
}

function loadFilms() {
    FilmFinder.byName(g_lastTitle, FilmFinder.Plot.Short, g_currentPage, function(films, total) {
        //Calculate the number of remaining films
        if (g_currentPage > 0) {
            g_filmsLeft -= parseInt(films.length);
        } else {
            g_filmsLeft = total - films.length;
        }

        //Add films to page
        addFilms(films);

        //Go to next page
        g_currentPage++;
        
        //Show button if more films avaible to load
        Utils.setVisible(g_loadMoreButton, (g_filmsLeft > 0));
    }, showError);
}

function addFilms(films) {
    for (var i = 0; i < films.length; ++i) {
        addFilm(films[i]);
    }
}

function addFilm(filmData) {
    //Apply upper case for first char of film type
    filmData.Type = Utils.upFirstChar(filmData.Type);

    var component = new FilmComponent(filmData);
    var element = component.buildElement();

    g_filmsBlock.appendChild(element);
}

function showError(error) {
    Utils.setVisible(g_errorBlock, true);
    g_errorBlock.innerText = error;
}

function clearPage() {
    while (g_filmsBlock.firstChild) {
        g_filmsBlock.removeChild(g_filmsBlock.firstChild);
    }
}

/*----------------[Events]----------------*/

function onLoadMore() {
    if (!g_lastTitle) {
        return;
    }

    if (g_filmsLeft <= 0) {
        return;
    }

    //Load films
    loadFilms();
}

function onSearchButtonClick() {
    var title = g_searchInput.value;

    if (!title) {
        return;
    }

    g_searchInput.value = "";
    
    //Clear the page
    clearPage();

    //Set last title to search request title
    g_lastTitle = title;

    //Set current page to start page
    g_currentPage = 0;

    //Hide error block
    Utils.setVisible(g_errorBlock, false);
    Utils.setVisible(g_loadMoreButton, false);

    //Load films
    loadFilms();
}

function onSearchInputKeyPress(event) {
    if (event.keyCode == 13) {
        g_searchButton.click();
    }
}